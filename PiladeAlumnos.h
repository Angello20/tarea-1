#pragma once
#include "Alumnos.h"

class PilaVaciaException {};

class Nodo{
  public:
    Nodo* Siguiente;
    Alumno Valor;
    Nodo();
    Nodo(Alumno& alumno);
};

class PilaAlumnos{
  private:
    Nodo* Cabeza;
  public:
    void Push(Alumno& alumno);
    Alumno Pop();
    Alumno Peek();
    bool PilaVacia();
};