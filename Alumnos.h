#pragma once 
#include <iostream>

using namespace std;

class Alumno
{
  private:

  string Nombre;
  string Apellido;

  public:

  Alumno(string nombre, string ape);
  Alumno();
  string GetNombre();
  void setNombre(string nombre);
  string GetApellido();
};