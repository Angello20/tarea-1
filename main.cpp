#include "Alumnos.h"
#include <iostream>
#include "PiladeAlumnos.h"

int main(){
  Alumno a1 = Alumno("Jose","Perez");
  Alumno a2 = Alumno("Luis","Baldano");
  Alumno a3 = Alumno("Eduardo","Enriquez");
  PilaAlumnos p1 = PilaAlumnos();
  p1.Push(a1);
  std::cout<<p1.Peek().GetNombre()<<std::endl;
  return 0;
}