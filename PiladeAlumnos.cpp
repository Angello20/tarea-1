#include "PiladeAlumnos.h"

Nodo::Nodo(Alumno& alumno){
  Siguiente = nullptr;
  Valor = alumno;
}
Nodo::Nodo(){
  Siguiente = nullptr;
}

void PilaAlumnos::Push(Alumno& alumno){
  Nodo* NuevoNodo = new Nodo(alumno);
  if (PilaVacia())
    Cabeza = NuevoNodo;
  else {
    NuevoNodo->Siguiente = Cabeza;
    Cabeza = NuevoNodo;
  }
}

Alumno PilaAlumnos::Pop(){
  if (PilaVacia())
    throw PilaVaciaException{};
  Alumno resultado = Cabeza -> Valor;
  Cabeza = Cabeza -> Siguiente;
  return resultado;
}

Alumno PilaAlumnos::Peek(){
  if (PilaVacia())
    throw PilaVaciaException{};
  return Cabeza->Valor;   
}

bool PilaAlumnos::PilaVacia(){
  return Cabeza == nullptr;
}
